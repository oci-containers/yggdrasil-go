# Unofficial yggdrasil-go OCI container
## About
This repo is used to build [yggdrasil-go](https://github.com/yggdrasil-network/yggdrasil-go) OCI container with sdnotify support for [podman automatic updates and rollback](https://www.redhat.com/sysadmin/podman-auto-updates-rollbacks). You can find built containers on [quay](https://quay.io/repository/oci-containers/yggdrasil-go). Following architectures supported: i386, amd64, armv7, aarch64, ppc64le and s390x.

Made by [shadowchain](https://gitlab.com/shdwchn10). Licensed under [MIT License](/LICENSE).

## How to use
0. You can use [yggdrasil-go OCI image](https://quay.io/repository/oci-containers/yggdrasil-go) with podman, docker or any other OCI engine. This instruction assumes that you are using podman and have installed it.
1. **!!!IMPORTANT!!!** rootful yggdrasil-go container **significantly increases your attack surface**. But rootless container **breaks** access to internal non-containerized services (like access to ssh from yggdrasil network) and IPv6 multicasting for local peers discovery. So rootless yggdrasil-go container is good only for public nodes or combining with other containers inside single [podman pod](https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods). Choose your poison.
1. Pull the container image with user it will be run as: `podman pull quay.io/oci-containers/yggdrasil-go:latest`.
1. Place [yggdrasil-go-podman.service](/yggdrasil-go-podman.service) file to `~/.config/systemd/user/` if you want rootless container or to `/etc/systemd/system/` if you want run container as root.
1. Replace all occurrences of `CHANGE_VOLUME` in the service file on your host volume (where the yggdrasil-go data is stored). If you use volume id instead of a path, then replace the second `CHANGE_VOLUME` on `%h/.local/share/containers/storage/volumes/<volume_id>` for rootless container or just remove it for rootful container.
1. Change `CHANGE_HOSTNAME` in the service file to a preferable container hostname.
1. Remove `--network=host` in the service file if you want rootless container or replace it on `-p <host_port>:<ctr_port>/tcp` if you need port forwarding for rootless container. You can specify `-p blah:blah` multiple times.
1. Remove all between `## IF ROOTLESS CUT HERE` lines in the service file if you want rootless container.
1. (Optional) You can also remove all hardening lines from the unit if you have problems with them.
1. Setup your firewall. Block any access to ports from yggdrasil and allow only what you need. Quick example for rootful container with firewalld on host system:

	```
	cp /usr/lib/firewalld/zones/public.xml /etc/firewalld/zones/yggdrasil.xml
	firewall-cmd --reload
	firewall-cmd --zone=yggdrasil --change-interface=tun0 # be sure that yggdrasil creates excatly tun0 interface!
	firewall-cmd --zone=yggdrasil --remove-service=ssh # if you dont need ssh
	firewall-cmd --runtime-to-permanent

	# You can also add some services to the yggdrasil zone:
	firewall-cmd --zone=yggdrasil --add-service=syncthing --add-service=syncthing-gui
	firewall-cmd --runtime-to-permanent
	```

1. Reload systemd: `systemctl --user daemon-reload` for a rootless container or `systemctl daemon-reload` for a rootful container.
1. After that you can enable and run the container: `systemctl --user enable --now yggdrasil-go-podman.service` or `systemctl enable --now yggdrasil-go-podman.service` respectively.
1. If you want to enable automatic updates: `systemctl --user enable --now podman-auto-update.timer` or `systemctl enable --now podman-auto-update.timer`. If the automatic update fails, podman will rollback the problematic update.

## Further hardening (for advanced users)
### SELinux
0. Don't try it on OSTree-based systems (Silverblue, CoreOS) until [this issue](https://github.com/coreos/fedora-coreos-tracker/issues/701) is fixed.
1. Read ["Use udica to build SELinux policy for containers"](https://fedoramagazine.org/use-udica-to-build-selinux-policy-for-containers/)
1. Install [udica](https://github.com/containers/udica/#installing)
1. Generate SELinux policy:

	```
	podman inspect <running_container_id> | udica --container-engine podman yggdrasil-go

	# And follow instructions from the udica output like in this example:
	semodule -i yggdrasil-go.cil /usr/share/udica/templates/{base_container.cil,net_container.cil,home_container.cil}
	# Don't forget to replace '--security-opt label=disable' on the
	# '--security-opt label=type:yggdrasil-go.process' parameter
	# in the systemd unit, reload systemd and restart service!
	```

### seccomp
1. Read ["Improving Linux container security with seccomp"](https://www.redhat.com/sysadmin/container-security-seccomp)
1. Install [oci-seccomp-bpf-hook](https://github.com/containers/oci-seccomp-bpf-hook)
1. Add this to the systemd unit to generate seccomp profile (works only as root): `--annotation io.containers.trace-syscall=of:<absolute_path_to_the_profile>`. Start container, wait 10-15 minutes and stop container. Remove annotation, place generated profile somewhere and add its absolute path to `ReadWritePaths=` in the service file
1. Add `--security-opt seccomp=<absolute_path_to_the_profile>` to the systemd unit
1. You can see some profile examples in the [seccomp directory](/seccomp)

## How to build locally
### Install dependencies
**Fedora Silverblue** already has required packages.

**Fedora/RHEL**:
```
sudo dnf install buildah git
```

**Ubuntu/Debian**:
```
sudo apt install buildah git
```

**Arch Linux**:
```
sudo pacman -S buildah git
```

### Build
Just run [build.sh](/build.sh). You can set some environment variables to change script behavior:

- `ARCHS` — build target architectures. If `ARCHS` is not set, the script builds container image **only for the host CPU architecture**.
- Set `USE_TMPFS` to `true` to build in `/tmp`. Otherwise the script will build the image **in the current directory**.

Example:
```
USE_TMPFS=true ARCHS="386 amd64 arm arm64 ppc64le s390x" ./build.sh
```

## TODO
- [ ] Container CI testing
- [ ] More DRY
- [ ] Create installer
- [x] Write SELinux policy for rootful container
- [x] Create seccomp profile for rootful container
- [ ] Find a way to make rootless yggdrasil-go more powerful. Patches are welcome
- [ ] Maybe more container tags on quay
- [ ] Find an easy way to automagically trigger CI on new yggdrasil-go releases
