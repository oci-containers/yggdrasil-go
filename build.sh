#!/usr/bin/env bash
# shellcheck disable=SC2250

set -Eeuxo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

HOST_ARCH="$(buildah info | jq '.host.arch' | tr -d '"')"
# ARCHS="386 amd64 arm arm64 ppc64le s390x"
ARCHS=${ARCHS:-$HOST_ARCH}

MANIFEST_NAME=${MANIFEST_NAME:-yggdrasil-go}
WORKDIR=${WORKDIR:-/src}
GOVERSION=${GOVERSION:-latest}

# shellcheck disable=SC2154
if [[ ${USE_TMPFS:-false} == true ]]; then
    tmp_dir="$(mktemp -d)"
    pushd "$tmp_dir"
fi

# shellcheck disable=SC2154
if [[ ${GITLAB_CI:-false} != true ]]; then
    git clone https://github.com/yggdrasil-network/yggdrasil-go
    if [[ -v PROJECT_REF ]]; then
        pushd yggdrasil-go
        git checkout "$PROJECT_REF"
        popd
    fi

    buildah manifest rm "$MANIFEST_NAME" 2>/dev/null || true
    buildah manifest create "$MANIFEST_NAME"

    source "${SCRIPTPATH}/go_cache.sh"
fi



for ARCH in $ARCHS; do
    ctr_builder=$(buildah from "docker.io/library/golang:${GOVERSION}")
    buildah copy "$ctr_builder" yggdrasil-go "$WORKDIR"
    buildah copy "$ctr_builder" go /go
    buildah run --workingdir "$WORKDIR" --env GOARCH="$ARCH" --env CGO_ENABLED=0 "$ctr_builder" -- /bin/sh -c "./build && go build -o /src/genkeys cmd/genkeys/main.go"


    ctr=$(buildah from --arch "$ARCH" alpine)

    buildah run "$ctr" -- apk add --no-cache socat
    
    buildah copy --from "$ctr_builder" "$ctr" "${WORKDIR}/yggdrasil" /usr/bin/yggdrasil
    buildah copy --from "$ctr_builder" "$ctr" "${WORKDIR}/yggdrasilctl" /usr/bin/yggdrasilctl
    buildah copy --from "$ctr_builder" "$ctr" "${WORKDIR}/genkeys" /usr/bin/genkeys
    buildah copy "$ctr" yggdrasil-go/contrib/docker/entrypoint.sh /usr/bin/entrypoint.sh
    
    buildah copy "$ctr" "${SCRIPTPATH}/podman-health-notifier" /usr/bin/podman-health-notifier
    buildah run "$ctr" -- sed -i '/^set -e$/a exec /usr/bin/podman-health-notifier &' /usr/bin/entrypoint.sh

    buildah config --arch "$ARCH" --label maintainer="dev@shdwchn.io" \
                   --volume /etc/yggdrasil-network --entrypoint /usr/bin/entrypoint.sh "$ctr"


    if [[ ${GITLAB_CI:-false} != true ]]; then
        buildah commit --manifest "$MANIFEST_NAME" "$ctr"
    else
        buildah commit "$ctr" "${MANIFEST_NAME}-${ARCH}"
    fi

    buildah rm "$ctr_builder"
    buildah rm "$ctr"
done



if [[ ${USE_TMPFS:-false} == true ]]; then
    popd
fi

